class Eleve {

    constructor(nom,prenom,age){
        this.id;
        this.nom = nom;
        this.prenom = prenom;
        this.age = age;
    }

    fromJson(json){
        this.id = json["id"];
        this.nom = json["nom"];
        this.prenom = json["prenom"];
        this.age = json["age"];
        this.ecole_id = json["ecole_id"];
    }

    getNom(){
        return this.nom;
    }

    setNom(nom){
        this.nom = nom;
    }

    getPrenom(){
        return this.prenom;
    }

    setPrenom(prenom){
        this.prenom = prenom;
    }

    getAge(){
        return this.age;
    }

    setAge(age){
        this.age = age;
    }

    isMageur(age){
        if(age >= 18 ){
            return true;
        }else{
            return false;
        }
    }
}

module.exports = Eleve;