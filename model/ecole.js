class Ecole {

    constructor(nom, eleves = []) {
        this.id;
        this.nom = nom;
        this.eleves = eleves;
    }

    fromJson(json){
        this.id = json["id"];
        this.nom = json["nom"];
        this.eleves = json["eleves"];
    }

    getID() {
        return this.id;
    }

    getNom() {
        return this.nom;
    }

    setNom(nom) {
        this.nom = nom;
    }

    addEleve(eleve) {
        this.eleves.push(eleve);
    }

    listEleve() {
        return this.eleves;
    }

    removeEleve(eleve) {
        this.eleves = this.eleves.filter(function (e) {
            return eleve !== e;
        });
    }

    nombreEleve(){
        return this.eleves.length;

    }

}

module.exports = Ecole;