const eleveModel = require('../model/eleve');
const db = require('../config/db');

async function getAllEleve(){
    try {
        const eleves = [];
        const result = await db.awaitQuery('SELECT * FROM eleve');
        for(let x = 0; x < result.length; x++){
            let eleve = new eleveModel();
            eleve.fromJson(result[x]);
            eleves.push(eleve);
        }
        return {"status":200,"data":eleves};
    }catch(e){
        return {"status": 400,"error":e.toString()};
    }
}

async function getEleveById(id){
    try {
        const result = await db.awaitQuery('SELECT * FROM eleve WHERE id=?', [id] );
        return {"status":200,"data":result};
    }catch(e){
        return {"status": 400,"error":e.toString()};
    }
}

async function getEleveByEcole(id){
    try {
        const result = await db.awaitQuery('SELECT * FROM eleve WHERE ecole_id=?', [id] );
        return {"status":200,"data":result};
    }catch(e){
        return {"status": 400,"error":e.toString()};
    }
}

async function RemoveEcoleFromEleve(id){
    try {
        await db.awaitQuery('UPDATE eleve SET ecole_id=NULL WHERE id=?', [id] );
        return {"status":200,"data":"l'utilisateur à était détacher de l'école"};
    }catch(e){
        return {"status": 400,"error":e.toString()};
    }
}

async function getDeleteById(id){
    try {
        await db.awaitQuery('DELETE FROM eleve WHERE id=?', [id] );
        return {"status":200,"data":"utilisateur supprimer avec succès"};
    }catch(e){
        return {"status": 400,"error":e.toString()};
    }
}

async function updateEleve(eleve){
    try {
        await db.awaitQuery('UPDATE eleve SET nom=?, prenom=?, age=? WHERE id=?', 
        [eleve.nom,eleve.prenom,eleve.age, eleve.id]);
        return {"status":200,"data":"utilisateur modifier avec succès"};
    }catch(e){
        return {"status": 400,"error":e.toString()};
    }
}

async function updateEcoleFromEleve(eleve){
    // await db.awaitQuery('UPDATE eleve SET nom=?, prenom=?, age=?, ecole_id=? WHERE id=?', 
    // [eleve.nom, eleve.prenom, eleve.age, eleve.ecole_id, eleve.id]);
    try {
        await db.awaitQuery('UPDATE eleve SET ecole_id=? WHERE id=?', 
        [eleve.ecole_id, eleve.id]);
        return {"status":200,"data":"ecole de l'utilisateur modifier avec succès"};
    }catch(e){
        return {"status": 400,"error":e.toString()};
    }
}

async function addEleve(eleve){
    try {
        await db.awaitQuery('INSERT INTO eleve (nom,prenom,age,ecole_id) VALUES (?,?,?,?)', 
        [eleve.nom, eleve.prenom, eleve.age, eleve.ecole_id]);
        return {"status":200,"data":"utilisateur ajouté avec succès"};
    }catch(e){
        return {"status": 400,"error":e.toString()};
    }
}

module.exports = {
    updateEleve,
    getDeleteById,
    getEleveById,
    addEleve,
    getAllEleve,
    getEleveByEcole,
    RemoveEcoleFromEleve,
    updateEcoleFromEleve
}