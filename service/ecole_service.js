const ecoleModel = require('../model/ecole');
const db = require('../config/db');

async function getAllEcole(){
    try {
        const ecoles = [];
        const result = await db.awaitQuery('SELECT * FROM ecole');
        for(let x = 0; x < result.length; x++){
            let ecole = new ecoleModel();
            ecole.fromJson(result[x]);
            ecoles.push(ecole);
        }
        return {"status":200,"data":ecoles};
    }catch(e){
        return {"status": 400,"error":e.toString()};
    }
}

async function getEcoleById(id){
    try {
        const result = await db.awaitQuery('SELECT * FROM ecole WHERE id=?', [id] );
        return {"status":200,"data":result};
    }catch(e){
        return {"status": 400,"error":e.toString()};
    }
}

async function getDeleteById(id){
    try {
        await db.awaitQuery('DELETE FROM ecole WHERE id=?', [id] );
        return {"status":200,"data":"ecole supprimer avec succès"};
    }catch(e){
        return {"status": 400,"error":e.toString()};
    }
}

async function updateEcole(ecole){
    try {
        await db.awaitQuery('UPDATE ecole SET nom=? WHERE id=?', 
        [ecole.nom, ecole.id]);
        return {"status":200,"data":"ecole modifier avec succès"};
    }catch(e){
        return {"status": 400,"error":e.toString()};
    }
}

async function addEcole(ecole){
    try {
        await db.awaitQuery('INSERT INTO ecole (nom) VALUES (?)', 
        [ecole.nom]);
        return {"status":200,"data":"ecole ajouter avec succès"};
    }catch(e){
        return {"status": 400,"error":e.toString()};
    }
}

module.exports = {
    getAllEcole,
    getEcoleById,
    getDeleteById,
    updateEcole,
    addEcole
}