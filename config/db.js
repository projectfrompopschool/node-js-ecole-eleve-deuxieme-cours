const mysql = require('mysql-await');
require("dotenv").config();

const db = mysql.createConnection({
    host: process.env.ADDRESS,
    user: process.env.USER,
    password: process.env.PASSWORD,
    database: process.env.DATABASE
})

console.log(process.env)

db.connect(function(err){
    if(err) throw err;
    // console.log("Connection OKAY");
});

module.exports = db;