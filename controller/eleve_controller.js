const eleveService = require('../service/eleve_service');

exports.getAll =  async (req, res) => {
    let result = await eleveService.getAllEleve();
    res.json(result);
};

exports.getById = async (req, res) => {
    let result = await eleveService.getEleveById(req.params["id"]);
    res.json(result);
};

exports.getEleveByEcole = async (req, res) => {
    let result = await eleveService.getEleveByEcole(req.params["id"]);
    res.json(result);
};

exports.deleteEcoleFromEleve = async (req, res) => {
    let result = await eleveService.RemoveEcoleFromEleve(req.params["id"]);
    res.json(result);
};

exports.deleteEleveByID = async (req, res) => {
    let result = await eleveService.getDeleteById(req.params["id"]);
    res.json(result);
};

exports.ajoutEleve = async (req, res) => {
    let eleve = new eleveModel();
    eleve.fromJson(req.body);
    let result = await eleveService.addEleve(eleve);
    res.json(result);
};

exports.miseAjourEcoleFromEleve =  async (req, res) => {
    let eleve = new eleveModel(req.body);
    eleve.fromJson(req.body);
    let result = await eleveService.updateEcoleFromEleve(eleve);
    res.json(result);
};

exports.miseAjourEleve =  async (req, res) => {
    let eleve = new eleveModel(req.body);
    eleve.fromJson(req.body);
    let result = await eleveService.updateEleve(eleve);
    res.json(result);
}