const ecoleService = require('../service/ecole_service');

exports.getAll = async (req, res) => {
    let result = await ecoleService.getAllEcole();
    res.json(result);;
};

exports.ecoleById = async (req, res) => {
    let result = await ecoleService.getEcoleById(req.params["id"]);
    res.json(result);
};

exports.deleteEcoleById = async (req, res) => {
    let result = await ecoleService.getDeleteById(req.params["id"]);
    res.json(result);
};

exports.ajoutEcole = async (req, res) => {
    let ecole = new ecoleModel();
    ecole.fromJson(req.body);
    let result = await ecoleService.addEcole(ecole);
    res.json(result);
};

exports.miseAjourEcole = async (req, res) => {
    let ecole = new ecoleModel(req.body);
    ecole.fromJson(req.body);
    let result = await ecoleService.updateEcole(ecole);
    res.json(result);
};