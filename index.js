const express = require('express');
const bodyParser = require('body-parser');
const eleveRouter = require('./route/eleve_route');
const ecoleRouter = require('./route/ecole_route');
const app = express();
const port = 3000;


app.use(bodyParser.json());
app.use("/eleve",eleveRouter);
app.use("/ecole",ecoleRouter);



app.get('/', (req, res) => {
    res.send('Hello World!')
})

app.listen(port, () => {
    console.log(`Example app listening on port ${port}`)
})