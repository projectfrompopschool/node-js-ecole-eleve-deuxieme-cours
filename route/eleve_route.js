const express = require('express');
const eleveService = require('../service/eleve_service');
const eleveModel = require('../model/eleve');
const eleveController = require('../controller/eleve_controller');
const router = express.Router();

router.get('/all', eleveController.getAll)

router.get('/by/:id', eleveController.getById)

router.get('/byecole/:id', eleveController.getEleveByEcole)

router.delete('/deleteecole/:id', eleveController.deleteEcoleFromEleve)

router.delete('/delete/:id', eleveController.deleteEleveByID)

router.post('/add', eleveController.ajoutEleve) 

////////////probleme de route/////////////////
router.put('/update/ecole',  eleveController.miseAjourEcoleFromEleve) 

/////////////update en fonction de l'objet, pas de l'adresse/////////////////////
router.put('/update/:id', eleveController.miseAjourEleve) 

module.exports = router;