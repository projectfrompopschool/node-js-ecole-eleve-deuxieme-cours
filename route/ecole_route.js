const express = require('express');
const ecoleService = require('../service/ecole_service');
const ecoleModel = require('../model/ecole');
const ecoleController = require('../controller/ecole_controller');
const router = express.Router();

router.get('/all', ecoleController.getAll);

router.get('/by/:id', ecoleController.ecoleById);

router.delete('/delete/:id',ecoleController.deleteEcoleById);

router.post('/add',ecoleController.ajoutEcole);

/////////////update en fonction de l'objet, pas de l'adresse/////////////////////
router.put('/update', ecoleController.miseAjourEcole);


module.exports = router;